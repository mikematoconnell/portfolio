

const PROJECT_EXPERIENCE_DATA = [
    {
        id: ,
        name: "Crwn Clothing",
        hosted: "https://storied-sherbet-7e2e35.netlify.app/",
        generalDescription: "Clothing website created by following React Udemy course by Zero to Mastery school. Allows users to select items and add them to a cart that can bought at a mock checkout.",
        buildDescription: "Built React fronted that includes TypeScript, React Redux, and React Persist. Backend is hosted by Google's Firebase/Firestore, which allows for Google Sign In as well as Restful API data query. Hosted with Netlify.",
        inProgress: true,
        startDate: "November 2022",
        endDate: ,
        typeOfProject: "Single Learning",
        image: ".assets/Crwn_Clothing_Home_page.png",
    },
    {
        id: ,
        name: "Personality Test",
        hosted: false,
        generalDescription: "Web application where users can sign in create a profile and take a variety of personality tests including Myers-Briggs and Hogwarts Sorting Hat. Users can add friends to chat about their results as well as display their result badges on their profile page for others to see.",
        buildDescription: "Build with a React fronted that incorporates React Redux and React Query for data management and persistance. ",
        inProgress: true,
        startDate: "October 2022",
        endDate: undefined,
        typeOfProject: "Pair Programmed",
        teammates: [
            {
                name: "James Egan",
                gitlab: "https://gitlab.com/James_Egan/personality-test"
            }
        ]
        image: ,
    },

    {
        id: 2,
        name: "Project Alpha",
        gitlab: "https://gitlab.com/mikematoconnell/project-alpha-apr",
        hosted: false,
        generalDescription: "Task creation website",
        buildDescription: ,
        inProgress: false,
        startDate: "June 2022",
        endDate: "June 2022",
        typeOfProject: "",
        image: ,
    },
    {
        id: 1,
        name: "Scrumptious Recipes",
        gitlab: "https://gitlab.com/mikematoconnell/scrumptious-recipes",
        hosted: false,
        generalDescription: "Recipe website that allows user to create and find recipes. It also allows users to add grocery items from recipes to a grocery list.",
        buildDescription: "Django monolith backend displayed using Django static pages.",
        inProgress: false,
        startDate: "May 2022",
        endDate: "June 2022",
        typeOfProject: "",
        image: ,
    }
]



// data format:
// {
//     id: ,
//     name: ,
//     gitlab:
//     hosted: ,
//     generalDescription: ,
//     buildDescription: ,
//     inProgress: ,
//     startDate: ,
//     endDate: ,
//     typeOfProject: ,
//     image: ,
// }
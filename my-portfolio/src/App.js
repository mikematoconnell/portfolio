import { Routes, Route } from "react-router-dom"

import HomePage from "./routes/homepage/homepage.component";
import ProfilePicture from "./components/profile-picture/profile-picture.component";

function App() {
  return (
    <Routes>
      <Route path="/" element={<ProfilePicture />} />
    </Routes>
  )
}

export default App;

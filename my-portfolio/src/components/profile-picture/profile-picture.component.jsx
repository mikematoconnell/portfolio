import { ProfilePictureContainer } from "./profile-picture.styles";

const ProfilePicture = () => {
    const profile_picture = "../../assets/profile_picture.jpg"
    console.log(profile_picture)
    return(
        <ProfilePictureContainer>
            <img src={require("../../assets/profile_picture.jpg")} alt="car dealership"/>
        </ProfilePictureContainer>
    )
}

export default ProfilePicture